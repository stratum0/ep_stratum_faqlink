var eejs = require('ep_etherpad-lite/node/eejs/');

exports.eejsBlock_indexWrapper = function (hook_name, args, cb) {
  args.content = args.content + eejs.require("ep_stratum0_faqlink/templates/indexpage.ejs");
  return cb();
}

exports.eejsBlock_editbarMenuLeft = function (hook_name, args, cb) {
  args.content = args.content + eejs.require("ep_stratum0_faqlink/templates/editbarButtons.ejs");
  return cb();
}

console.log("ep_stratum0_faqlink loaded.");

// vim: set et:
